#!/system/xbin/busybox ash

export curdate=`date "+%m-%d-%Y"`

BB="/system/xbin/busybox"

STABLE="http://goo.im/devs/Ayysir/d2tmo/ASDK/ASDK-AOSP-3.4.x-$curdate-EXP.zip"

$BB mkdir -p /sdcard/ASDK

while [ 1 ]; do
echo
echo "ASDK Updater"
echo "------------------"
echo "1) latest stable (recommended)"
echo "2) latest experimental (not available)" 
echo "3) check/display versions" 
echo
echo "FLASH AT YOUR OWN RISK. I'm not responsible for my mistakes or yours. ;)"
echo
echo -n "Please enter a number between 1 and 3 (or press enter to exit): "

read option
case $option in
  1)
	$BB wget --no-check-certificate $STABLE -O /sdcard/ASDK/
	echo; echo -n "Downloading stable "
	break
	;;
  2) 
	$BB wget -q -O /sdcard/lk/loc $EXP
	echo; echo -n "Downloading experimental "
	break
	;;
  3) 
  	echo "Please wait..."
	vstable=`$BB wget -q -O - $STABLE | $BB awk '{ print \$3 }'`
	vexp=`$BB wget -q -O - $EXP | $BB awk '{ print \$3 }'`
	echo; echo ">>> Current version: $CUR, Latest stable: $vstable, Exp: $vexp"	
	sleep 2
	;;
  *)
	echo "BYE"
	exit
	;;
esac
done

